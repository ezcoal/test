//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.5.0;
contract BettingContract{
    struct betInfo{
        address payable bettor;
        uint option;
    }
    betInfo[] betData;
    address payable owner;
    address payable[]  winners;
    uint256 betAmount;
    uint256 matchStartTime;
    uint256 matchEndTime;
    uint betLimit;
    modifier checkOwner{
        require(msg.sender==owner,'Only owner can access this');
        _;
    }
    modifier checkStartTime{
        require(now<matchStartTime,'Sorry , Match has started.');
        _;
    }
    modifier checkEndTime{
        require(now>matchEndTime,'Match is progressing');
        _;
    }
    modifier checkAmount{
        require(msg.value == betAmount*1 ether,'Bet Amount should be equal');
        _;
    }
    modifier checkLimit{
        require(betData.length<betLimit,'Limit Reached');
        _;
    }
    constructor(uint256 amount,uint256 startTime,uint256 endTime,uint bLimit) public{
        owner = msg.sender;
        betAmount = amount;
        matchStartTime = startTime;
        matchEndTime = endTime;
        betLimit = bLimit;
    }
      event winnersEvent(address indexed,uint256);
      event destoryedEvent(address,address);
      event betEvent(address indexed,uint);
    function onBet(uint option) public checkLimit checkAmount checkStartTime payable{
       require(msg.sender!=owner,'owner cannot bet');
       betData.push(betInfo(msg.sender,option));
       emit betEvent(msg.sender,betData.length);
    }
    function checkContractBalance() public view checkOwner returns(uint256)
    {
        return address(this).balance;
    }
    function findWinners(uint option) public checkOwner{
        for(uint i = 0;i<betData.length;i++)
        {
            if(betData[i].option==option)
            winners.push(betData[i].bettor);
        }
    }
    function transferForWinners(uint option) public payable checkOwner checkEndTime
    {
        findWinners(option);
        uint256 contractBalance = address(this).balance;
        uint noOfWinners = winners.length;
        uint256 cutForWinners = contractBalance/noOfWinners;
        for(uint i = 0;i<winners.length;i++)
        {
            winners[i].transfer(cutForWinners);
            emit winnersEvent(winners[i],cutForWinners);
        }
    }

    function destroyContract() public
    {
        emit destoryedEvent(owner,address(this));
        selfdestruct(owner);
    }
    function betDetails() public view returns(uint _betamount,uint256 _starttime,uint256 _endtime,uint _betlimit,uint _noofbets)
    {
        _betamount = betAmount;
        _starttime = matchStartTime;
        _endtime = matchEndTime;
        _betlimit = betLimit;
        _noofbets = betData.length;
    }
}

contract contractStorage{
    uint id=0;
    mapping(uint=>BettingContract) public contracts;
    address payable owner;
     modifier checkOwner{
        require(msg.sender==owner,'Only owner can access this');
        _;
    }
    event createdEvent(address indexed,uint);
    constructor() public
    {
        owner = msg.sender;
    }
    function deposit() public payable{
    }
    function createContract(uint256 amount,uint256 startTime,uint256 endTime,uint betLimit) public checkOwner
    {
        BettingContract newContract = new BettingContract(amount,startTime,endTime,betLimit);
        contracts[id] = newContract;
        id++;
        emit createdEvent(address(newContract),id-1);
    }
    function contractBalance(uint _id) public view returns(uint256)
    {
        return contracts[_id].checkContractBalance();
    }
    function transfer(uint _id,uint _option) public
    {
        contracts[_id].transferForWinners(_option);
    }
    function deleteContract(uint _id) public{
        contracts[_id].destroyContract();
         delete contracts[_id];
    }
    function withdrawFromStorage(uint amount) public payable{
        owner.transfer(amount*1 ether);
    }
    function destoryStorage() public checkOwner{
        selfdestruct(owner);
    }
    function checkLogin() public view returns(bool){
        if(msg.sender==owner)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}