const express = require("express");
const solc = require("solc");
const path = require("path");
const fs = require("fs");
const bcrypt = require("bcrypt");
const AdminData = require("../db/models/AdminModel");
const MatchData = require("../db/models/MatchModel");

const adminRouter = express.Router();

//checking login
adminRouter.post("/login", (req, res, next) => {
  let username = req.body.username;
  let password = req.body.password;
  
  AdminData.findOne((err, result) => {
    if (
      bcrypt.compareSync(username, result.username) &&
      bcrypt.compareSync(password, result.password)
    ) {
      req.session.admin = true;
      res.send("true");
    } else {
      res.send("false");
    }
  });
});

adminRouter.get("/home", (req, res) => {
  if (req.session.admin != true) {
    res.render("adminlogin");
  } else {
    res.render("adminhome");
  }
});

adminRouter.route("/add").get((req, res) => {
  if (req.session.admin != true) {
    res.render("adminlogin");
  } else {
    res.render("addBetInfo");
  }
});

adminRouter.route("/add/bet").post((req, res, next) => {
  let reqData = JSON.parse(JSON.stringify(req.body));

  //Converting start time and end time

  let startdate = new Date(reqData.startdate);
  startdate.setHours(Number.parseInt(reqData.starttime.split(":")[0]));
  startdate.setMinutes(Number.parseInt(reqData.starttime.split(":")[1]));

  let enddate = new Date(reqData.enddate);
  enddate.setHours(Number.parseInt(reqData.endtime.split(":")[0]));
  enddate.setMinutes(Number.parseInt(reqData.endtime.split(":")[1]));

  //checking the duplicate entry

  MatchData.find({
    MatchName: reqData.mname,
    TeamA: reqData.teamA,
    TeamB: reqData.teamB,
    StartTime: startdate.getTime(),
    Status: "OPEN",
  }).then((data) => {
    if (data.length != 0) {
      res.send("Duplicate Data Found");
    } else {
      //creating a contract using given data

      web3.eth.getAccounts(function (error, result) {
        // console.log(result[0]);
        contractInstance.methods
          .createContract(
            Number.parseInt(reqData.betamount),
            Number.parseInt(startdate.getTime()),
            Number.parseInt(enddate.getTime()),
            Number.parseInt(reqData.maxlimit)
          )
          .send({
            from: result[0],
            gas: 1000000,
          })
          .then((data, err) => {
            if (err) {
              console.log(err.message);
              res.send("Failed to create contract");
            } else {
              let matchData = new MatchData({
                MatchName: reqData.mname,
                TeamA: reqData.teamA,
                TeamB: reqData.teamB,
                Index: data.events.createdEvent.returnValues[1],
                StartTime: startdate.getTime(),
                Status: "OPEN",
              });

              matchData.save().then((saveData) => {
                res.send("Contract created Successfully");
              });
            }
          });
      });
    }
  });
});

adminRouter.route("/getallmatches").get((req, res) => {
  if (req.session.admin != true) {
    res.render("adminlogin");
  } else {
    MatchData.find({}).then((data) => {
      console.log(data);
    });
  }
});

adminRouter.route("/deleteEvent").get((req, res) => {
  web3.eth.getAccounts(function (error, result) {
    let str = contractInstance.methods.deleteContract(0).send({
      from: result[0],
      gas: 1000000,
    });

    console.log(str);
  });
});

adminRouter.route("/viewEvent").get((req, res) => {
  web3.eth.getAccounts(function (error, result) {
    contractInstance.methods
      .contracts(0)
      .call({
        from: result[0],
        gas: 1000000,
      })
      .then((data) => {
        conAdd = data;
      });
  });
});

module.exports = adminRouter;
