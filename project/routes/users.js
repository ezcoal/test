var express = require("express");
var router = express.Router();
const MatchData = require("../db/models/MatchModel");
const session = require("express-session");

/* GET users listing. */

router.get("/index", (req, res) => {
  if (req.session.login == true) {
    let ongoingmatchdata = new Array();
    let pastmatchdata = new Array();
    MatchData.find().then((data) => {
      let matchdatapromise = new Promise((resolve, reject) => {
        data.forEach((item) => {
          if (item.StartTime < Date.now()) {
            pastmatchdata.push(item);
            item.Status = "Closed";
            MatchData.updateOne({ _id: item._id }, item).then(
              (onUpdate, onUpdateErr) => {
                // console.log(onUpdate);
              }
            );
          } else {
            ongoingmatchdata.push(item);
          }
        });
      });

      res.render("index", {
        title: "Football Betting - Home",
        ongoingmatchdata: ongoingmatchdata,
        pastmatchdata: pastmatchdata,
      });
    });
  } else {
    res.redirect("/");
  }
});

router.get("/onbet", (req, res, next) => {
  let id = Number.parseInt(req.query.id);
  web3.eth.getAccounts((err, accounts) => {
    res.render("onbet", {
      accounts: accounts,
    });
  });
});

router.get("/getmatchdetails", (req, res, next) => {
  let id = Number.parseInt(req.query.id);
  res.render("pastmatchdetails");
});

router.get("/", function (req, res, next) {
  if (req.query.flag == "true" || req.session.login == true) {
    req.session.login = true;
    res.redirect("/index");
  } else {
    res.render("login");
  }
});

router.get("/logout", (req, res) => {
  req.session.destroy();
  res.redirect("/");
});

module.exports = router;
