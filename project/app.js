var createError = require('http-errors');
var express = require('express');
var cors = require('cors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var Web3 = require('web3');
var session = require('express-session');

web3 = new Web3('http://127.0.0.1:8545');
chalk = require('chalk');
db = require('./db/connect');

var contractJSON = require('./build/contracts/contractStorage.json');
var contractABI = contractJSON.abi;
var contractAddress = contractJSON.networks['5777'].address;

contractInstance = new web3.eth.Contract(contractABI, contractAddress);


var usersRouter = require('./routes/users');
var adminRouter = require('./routes/admin');
var app = express();

// view engine setup

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(cors());
app.use(cookieParser());
app.use(session({
    secret: "Shh, its a secret!",
saveUninitialized:true,
resave:false
}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/admin', adminRouter);
app.use('/', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;