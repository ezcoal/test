import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: "app-adminheader",
  templateUrl: "./adminheader.component.html",
  styleUrls: ["./adminheader.component.css"],
})
export class AdminheaderComponent implements OnInit {
  constructor(
    public router:Router
  ) {}

  ngOnInit(): void {
    if (localStorage.getItem('isLogin') != "true")
    {
      this.router.navigate(['/admin']);
      }
  }

  onClickLogout()
  {
    localStorage.clear();
    this.router.navigate(['/admin']);
  }
}
