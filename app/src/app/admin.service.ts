import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AdminService {

  url: String = "http://localhost:3000/admin";
  constructor(
    public http:HttpClient
  ) { }

  checkLogin(username,password)
  {
    return this.http.post(this.url + '/login', {
      username: username,
      password: password
    });
  }
}
