import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminaddbetinfoComponent } from './adminaddbetinfo.component';

describe('AdminaddbetinfoComponent', () => {
  let component: AdminaddbetinfoComponent;
  let fixture: ComponentFixture<AdminaddbetinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminaddbetinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminaddbetinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
