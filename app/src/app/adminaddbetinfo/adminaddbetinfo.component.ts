import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { typeofExpr } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-adminaddbetinfo',
  templateUrl: './adminaddbetinfo.component.html',
  styleUrls: ['./adminaddbetinfo.component.css']
})
export class AdminaddbetinfoComponent implements OnInit {

  public submitted: boolean = false;
  public flagsamedate: boolean = true;
  public addBetform: FormGroup;
  constructor(
    public adminService: AdminService,
    public formBuilder:FormBuilder
  ) { 
    this.addBetform = formBuilder.group({
      mname: ['', Validators.required],
      teamA: ['', Validators.required],
      teamB: ['', Validators.required],
      betamount: [, [Validators.required, Validators.pattern(/^[0-9]*$/)]],
      startdate: [,Validators.required],
      starttime: [Validators.required],
      startenddate: [true],
      enddate: [Validators.required],
      endtime: [Validators.required],
      maxlimit: [, [Validators.required, Validators.pattern(/^[0-9]*$/)]]
      })
  }
  
  get f()
  {
    return this.addBetform.controls;
  }

  ngOnInit(): void {
    if (this.flagsamedate == true)
    {
      this.f.enddate.disable();
      }
  }

  formControlCheck()
  {
    //startdate check
    if (typeof this.f.startdate.value != "string" || this.f.startdate.value.length<10)
    {
      
      this.f.startdate.setErrors({
        required: true
      });
    }

    //starttime check
    
    if (typeof this.f.starttime.value != "string" || this.f.starttime.value.length < 5) {

      this.f.starttime.setErrors({
        required: true
      });
    }

    //enddate check
    if (typeof this.f.enddate.value != "string" || this.f.enddate.value.length < 10) {

      this.f.enddate.setErrors({
        required: true
      });
    }

    //endtime check
    if (typeof this.f.endtime.value != "string" || this.f.endtime.value.length < 5) {

      this.f.endtime.setErrors({
        required: true
      });
    }
  }



  onSubmit()
  {
    this.submitted = true;
    this.formControlCheck();
    
    if (this.addBetform.invalid != true)
    {
      
    }
    else
    {
      
      }
  }

}
